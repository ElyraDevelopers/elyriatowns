package com.elyriacraft.plugins.ElyriaTowns.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.elyriacraft.plugins.ElyriaCore.utils.PayInventory;
import com.elyriacraft.plugins.ElyriaCore.utils.StringUtils;
import com.elyriacraft.plugins.ElyriaTowns.EtMainClass;
import com.elyriacraft.plugins.ElyriaTowns.game.Town;
import com.elyriacraft.plugins.ElyriaTowns.game.players.EtPlayer;
import com.elyriacraft.plugins.ElyriaTowns.game.players.Lord;
import com.elyriacraft.plugins.ElyriaTowns.game.rules.KingdomRule;
import com.elyriacraft.plugins.ElyriaTowns.messages.ErrorMessage;
import com.elyriacraft.plugins.ElyriaTowns.messages.NotifyMessage;
import com.elyriacraft.plugins.ElyriaTowns.permissions.Perm;

public class PlayerTownCmd implements CommandExecutor{

	@SuppressWarnings("unused")
	private EtMainClass plugin = (EtMainClass)EtMainClass.getInstance();
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("Town")) {
			Player p = (Player)sender;
			EtPlayer es = new EtPlayer(sender.getName());
			if(args.length == 0) {
				NotifyMessage.DO_HELP.sendTo(sender, "{LABEL}|" + label);
				return false;
			}
			switch(args[0]) {
			case "join": case "j":
				if(!es.hasPerm(Perm.PLAYER_TOWN_JOIN)) {
					ErrorMessage.NO_PERMISSION.sendTo(sender);
					return false;
				}
				if(args.length > 2) {
					ErrorMessage.MISSING_ARGUMENT.sendTo(sender, "{ARG}|the name of the town to join");
					return false;
				}
				if(es.hasTown()) {
					ErrorMessage.MUST_LEAVE_CURRENT_TOWN.sendTo(sender);
					return false;
				}
				Town[] towns = Town.byName(args[1]);
				if(towns.length < 1){
					ErrorMessage.NO_TOWN_FOUND_WITH_NAME.sendTo(sender);
					return false;
				}
				if(towns.length > 1) {
					ErrorMessage.MULTIPLE_TOWNS_FOUND_WITH_NAME.sendTo(sender, "{NAME}|" + args[1], "{TOWNS}|" + StringUtils.stringArraytoString(Town.nameList(towns)));
					return false;
				}
				if(towns[0].getKingdom() != es.getKingdom()) {
					ErrorMessage.TOWN_NOT_IN_YOUR_KINGDOM.sendTo(sender);
					return false;
				} else {
					es.setTown(towns[0]);
					NotifyMessage.JOINED_TOWN.sendTo(sender, "{TOWN}|" + towns[0].getName());
					return true;
				}
				
			case "leave": case "l":
				if(!es.hasPerm(Perm.PLAYER_TOWN_LEAVE)) {
					ErrorMessage.NO_PERMISSION.sendTo(sender);
					return false;
				}
				if(!es.hasTown()) {
					ErrorMessage.NO_TOWN_TO_LEAVE.sendTo(sender);
					return false;
				}else {
					es.setTown(null);
					return true;
				}
			case "create": case "new": case "n":
				if(!es.hasPerm(Perm.PLAYER_TOWN_CREATE)) {
					ErrorMessage.NO_PERMISSION.sendTo(sender);
					return false;
				}
				if(args.length < 2){
					ErrorMessage.MISSING_ARGUMENT.sendTo(sender, "{ARG}|the name for your new town");
					return false;
				} else {
					int cost = (int) es.getKingdom().getRules().getRule(KingdomRule.NEW_TOWN_COST);
					PayInventory pay = new PayInventory(plugin, "InventoryName", 9, cost, p.getName(), new PayInventory.BoughtItemHandler() {
			            @Override
			            public void recieve(Player player) {
			                new Town(player.getName(), new EtPlayer(player).getKingdom(), new Lord(player));
			                NotifyMessage.NEW_TOWN.sendTo(player);
			            }
			            @Override
			            public void notPayed(Player player) {
			                 ErrorMessage.DID_NOT_PAY_ENOUGH.sendTo(player);
			            }
			        });
					pay.open(p);
					return true;
				}
			case "claim": case "c":
				if(es.hasPerm())
				
			default:
				NotifyMessage.DO_HELP.sendTo(sender, "{LABEL}|" + label);
				return false;
			}
			
		}
		return false;
	}

}
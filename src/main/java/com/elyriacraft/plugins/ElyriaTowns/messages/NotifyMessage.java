package com.elyriacraft.plugins.ElyriaTowns.messages;

import java.util.regex.Pattern;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.elyriacraft.plugins.ElyriaCore.plugin.messages.IMessagePath;
import com.elyriacraft.plugins.ElyriaCore.plugin.messages.MessageRetriever;
import com.elyriacraft.plugins.ElyriaTowns.EtMainClass;

public enum NotifyMessage implements IMessagePath{

	STEPED_OVER_KINGDOM_BORDER("{TAG}&3 You just crossed the border to the kingdom {KINGDOM}!"),
	STEPED_OVER_TOWN_BORDER("{TAG}&3 You just entered the town {TOWN} with {CITIZENS} citizens!"),
	NEW_TOWN("{TAG}&2 You successfully created a new town. Do &6/lord name <Name> &2to set a name for your town! Its currently named the same as you"),
	LEFT_TOWN("{TAG}&2 You successfully left your town {TOWN}! Be careful, Not being in a town means you have no way to buy/sell with others or travel by train!"),
	JOINED_TOWN("{TAG}&2 You succesfully joined the town {TOWN}! You can read their ruleset by doing &6/town rules"),
	DO_HELP("{TAG}&4 Do &6/{LABEL} help &4for help"),
    KINGDOM_RULE_READABLE("In {NAME} it costs {NEWTOWNCOST} {CURRENCY} to start a new town, and it costs {DAILYTAX} {CURRENCY} in daily taxes pr. citizen. A town must have a minimum of {MINIMUMCITIZENS} players (exluding the lord), but you cannot exeed {MAXIMUMCITIZENS}. You {RAIDINTERNALTOWNS} raid town in your own kingdom, and you {ENEMYINTERNALTOWNS} enemy them.");

    private final String PREFIX = "messages.generics.";
    
    private String path;
    private boolean isList;
    String defaultMessage;
    private EtMainClass plugin = (EtMainClass)EtMainClass.getInstance();
    
    private NotifyMessage(String defaultMessage) {
            this(defaultMessage, false);
    }
    
    private NotifyMessage(String defaultMessage, boolean isList) {
            this.path = this.toString().toUpperCase();
            this.isList = isList;
            this.defaultMessage = defaultMessage;
    }
    
    @Override
    public final String getPrefix() {
            return PREFIX;
    }

    @Override
    public final String getMessagePath() {
            return this.path;
    }
    
    @Override
    public final boolean isList() {
            return this.isList;
    }
    
    public final void sendTo(CommandSender sender, String... replacements) {
    	MessageRetriever messageRetriever = EtMainClass.getInstance().getMessageHandler();
    	String[] messages = messageRetriever.getMessage(this);
    	EtMainClass.getInstance().getLog().debug("messages length: " + messages.length);
    	for (String message : messages) {
    		String newMessage = message;
    		if (replacements.length > 0) {
    			for (String curString : replacements) {
    				plugin.getLog().debug("curString: " + curString);
    				String[] curStringSplit = curString.split(Pattern.quote("|"));
    				newMessage = newMessage.replace(curStringSplit[0], curStringSplit[1]);
    			}
    		}
    		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', newMessage.replace("{TAG}", ChatColor.GOLD + "[" + EtMainClass.getPrefix() + "] ")));
    	}
    }
    public final String[] get(String... replacements) {
    	MessageRetriever messageRetriever = EtMainClass.getInstance().getMessageHandler();
    	String[] messages = messageRetriever.getMessage(this);
    	String[] returnMessages = new String[]{};
    	EtMainClass.getInstance().getLog().debug("messages length: " + messages.length);
    	for (String message : messages) {
    		int i = 0;
    		String newMessage = message;
    		if (replacements.length > 0) {
    			for (String curString : replacements) {
    				plugin.getLog().debug("curString: " + curString);
    				String[] curStringSplit = curString.split(Pattern.quote("|"));
    				newMessage = newMessage.replace(curStringSplit[0], curStringSplit[1]);
    			}
    		}
    		newMessage = ChatColor.translateAlternateColorCodes('&', newMessage.replace("{TAG}", ChatColor.GOLD + "[" + EtMainClass.getPrefix() + "] "));
    		returnMessages[i] = newMessage;
    	}
    	return returnMessages;
    }
	public void addDefaults() {
    	MessageRetriever messageRetriever = EtMainClass.getInstance().getMessageHandler();
		NotifyMessage[] Gmes = NotifyMessage.values();
		String curPath;
		String curDefault;
		for(NotifyMessage curGmes : Gmes){
			curPath = curGmes.getPrefix() + curGmes.getMessagePath();
			curDefault = curGmes.defaultMessage;
			((EtMainClass)EtMainClass.getInstance()).getMessageHandler().getCustomFileManager().getConfig().addDefault(curPath, curDefault);
		}
		messageRetriever.getCustomFileManager().getConfig().options().copyDefaults(true);
	}
}

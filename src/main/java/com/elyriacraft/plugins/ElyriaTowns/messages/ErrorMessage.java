package com.elyriacraft.plugins.ElyriaTowns.messages;

import java.util.regex.Pattern;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.elyriacraft.plugins.ElyriaCore.plugin.messages.IMessagePath;
import com.elyriacraft.plugins.ElyriaCore.plugin.messages.MessageRetriever;
import com.elyriacraft.plugins.ElyriaTowns.EtMainClass;

public enum ErrorMessage implements IMessagePath{
	
	
	DID_NOT_PAY_ENOUGH("{TAG}&4 You didnt pay enough!"),
	MUST_BE_IN_OWN_KINGDOM("{TAG}&4 You must be standing in your own kingdom to {DO}!"),
	NO_PERMISSION("{TAG}&4 You dont have permission to do that!"),
	/**
	 * @Replacements {ARG} - the thing that is missing (located direcly after a "missing" by default) <br>example: "the town to join"
	 */
	MISSING_ARGUMENT("{TAG}&4 Too few arguments! missing {ARG}!"),
	NO_TOWN_TO_LEAVE("{TAG}&4 You cannot leave a town as you are in none!"),
	TOWN_NOT_IN_YOUR_KINGDOM("{TAG}&4 That town is not in the same kingdom as you!"),
	NO_TOWN_FOUND_WITH_NAME("{TAG}&4 No town was found by that name!"),
	MULTIPLE_TOWNS_FOUND_WITH_NAME("{TAG}&4 Multiple towns where found with the name '{NAME}': {TOWNS}"),
	MUST_LEAVE_CURRENT_TOWN("{TAG}&4 You must leave your current town before joining a new one!");

    private final String PREFIX = "messages.notify.";
    
    private String path;
    private boolean isList;
    String defaultMessage;
    private EtMainClass plugin = (EtMainClass)EtMainClass.getInstance();
    
    private ErrorMessage(String defaultMessage) {
            this(defaultMessage, false);
    }
    
    private ErrorMessage(String defaultMessage, boolean isList) {
            this.path = this.toString().toUpperCase();
            this.isList = isList;
            this.defaultMessage = defaultMessage;
    }
    
    @Override
    public final String getPrefix() {
            return PREFIX;
    }

    @Override
    public final String getMessagePath() {
            return this.path;
    }
    
    @Override
    public final boolean isList() {
            return this.isList;
    }
    
    public final void sendTo(CommandSender sender, String... replacements) {
    	MessageRetriever messageRetriever = EtMainClass.getInstance().getMessageHandler();
    	String[] messages = messageRetriever.getMessage(this);
    	EtMainClass.getInstance().getLog().debug("messages length: " + messages.length);
    	for (String message : messages) {
    		String newMessage = message;
    		if (replacements.length > 0) {
    			for (String curString : replacements) {
    				plugin.getLog().debug("curString: " + curString);
    				String[] curStringSplit = curString.split(Pattern.quote("|"));
    				newMessage = newMessage.replace(curStringSplit[0], curStringSplit[1]);
    			}
    		}
    		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', newMessage.replace("{TAG}", ChatColor.GOLD + "[" + EtMainClass.getPrefix() + "] ")));
    	}
    }
    public final String[] get(String... replacements) {
    	MessageRetriever messageRetriever = EtMainClass.getInstance().getMessageHandler();
    	String[] messages = messageRetriever.getMessage(this);
    	String[] returnMessages = new String[]{};
    	EtMainClass.getInstance().getLog().debug("messages length: " + messages.length);
    	for (String message : messages) {
    		int i = 0;
    		String newMessage = message;
    		if (replacements.length > 0) {
    			for (String curString : replacements) {
    				plugin.getLog().debug("curString: " + curString);
    				String[] curStringSplit = curString.split(Pattern.quote("|"));
    				newMessage = newMessage.replace(curStringSplit[0], curStringSplit[1]);
    			}
    		}
    		newMessage = ChatColor.translateAlternateColorCodes('&', newMessage.replace("{TAG}", ChatColor.GOLD + "[" + EtMainClass.getPrefix() + "] "));
    		returnMessages[i] = newMessage;
    	}
    	return returnMessages;
    }
	public void addDefaults() {
    	MessageRetriever messageRetriever = EtMainClass.getInstance().getMessageHandler();
		NotifyMessage[] Gmes = NotifyMessage.values();
		String curPath;
		String curDefault;
		for(NotifyMessage curGmes : Gmes){
			curPath = curGmes.getPrefix() + curGmes.getMessagePath();
			curDefault = curGmes.defaultMessage;
			((EtMainClass)EtMainClass.getInstance()).getMessageHandler().getCustomFileManager().getConfig().addDefault(curPath, curDefault);
		}
		messageRetriever.getCustomFileManager().getConfig().options().copyDefaults(true);
	}
}

package com.elyriacraft.plugins.ElyriaTowns.game;

import org.bukkit.Chunk;
import org.bukkit.ChunkSnapshot;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;

import com.elyriacraft.plugins.ElyriaCore.utils.ChunkUtils;
import com.elyriacraft.plugins.ElyriaTowns.EtMainClass;

public class Claim implements Chunk{

	private EtMainClass plugin = (EtMainClass)EtMainClass.getInstance();
	private FileConfiguration co = plugin.getStorageMan().getClaimFile().getConfig();
	private Chunk c;
	private String cString;

	public Claim(Location l){
		c = l.getChunk();
		cString = ChunkUtils.toString(c);
	}
	public Claim(Chunk c){
		this.c = c;
		cString = ChunkUtils.toString(c);
	}
	public boolean isClaimed(){
		if(co.contains(cString)){
			return true;
		}
		return false;
	}
	public boolean claim(Town t){
		return t.claim(this);
	}
	public Kingdom getKingdom(){
		int x = c.getX();
		int z = c.getZ();
		if(plugin.getStorageMan().getKingdomFile().getConfig().getStringList("MIDDLE.claims").contains(cString)){
			return Kingdom.MIDDLE;
		}
		if(z >= 0 && x <= 0){
			return Kingdom.SOUTH; 
		}
		if(z <= 0 && x >= 0){
			return Kingdom.NORTH;
		}
		if(z < 0 && x < 0){
			return Kingdom.WEST;
		}
		if(z > 0 && x > 0){
			return Kingdom.EAST;
		}
		return null;
	}
	@Override
	public String toString(){
		return cString;
	}
	public static Claim fromString(String s){
		return new Claim(ChunkUtils.fromString(s));
	}
	@Override
	public int getX() {
		return c.getX();
	}
	@Override
	public int getZ() {
		return c.getZ();
	}
	@Override
	public World getWorld() {
		return c.getWorld();
	}
	@Override
	public Block getBlock(int x, int y, int z) {
		return c.getBlock(x, y, z);
	}
	@Override
	public ChunkSnapshot getChunkSnapshot() {
		return c.getChunkSnapshot();
	}
	@Override
	public ChunkSnapshot getChunkSnapshot(boolean includeMaxblocky, boolean includeBiome, boolean includeBiomeTempRain) {
		return getChunkSnapshot(includeMaxblocky, includeBiome, includeBiomeTempRain);
	}
	@Override
	public Entity[] getEntities() {
		return c.getEntities();
	}
	@Override
	public BlockState[] getTileEntities() {
		return c.getTileEntities();
	}
	@Override
	public boolean isLoaded() {
		return c.isLoaded();
	}
	@Override
	public boolean load(boolean generate) {
		return c.load(generate);
	}
	@Override
	public boolean load() {
		return c.load();
	}
	@Override
	public boolean unload(boolean save, boolean safe) {
		return c.unload(save, safe);
	}
	@Override
	public boolean unload(boolean save) {
		return c.unload(save);
	}
	@Override
	public boolean unload() {
		return c.unload();
	}
}

package com.elyriacraft.plugins.ElyriaTowns.game.rules;

import org.bukkit.configuration.ConfigurationSection;

import com.elyriacraft.plugins.ElyriaTowns.game.Town;
import com.elyriacraft.plugins.ElyriaTowns.messages.NotifyMessage;
import com.elyriacraft.plugins.ElyriaTowns.storage.ConfigManager;

public class TownRules {

	private Town t;
	private ConfigurationSection cs;
	public TownRules(Town t){
		this.t = t;
		cs = t.getConfSec().createSection("Rules");
	}
	public void setRule(KingdomRule rule, Object value){
		cs.set(rule.toString(), value);
	}
	public Object getRule(KingdomRule rule){
		return cs.get(rule.toString());
	}
	public String getHumanReadable(){
		String eit = (Boolean.valueOf(String.valueOf(getRule(KingdomRule.ENEMY_INTERNAL_TOWNS)))? "can": "cannot");
		String rit = (Boolean.valueOf(String.valueOf(getRule(KingdomRule.RAID_INTERNAL_TOWNS)))? "can": "cannot");
		String maxc = String.valueOf(getRule(KingdomRule.MAX_CITIZENS));
		String minc = String.valueOf(getRule(KingdomRule.MIN_CITIZENS));
		String dtpc = String.valueOf(getRule(KingdomRule.DAILY_TOWN_TAX_PR_CITIZEN));
		String ntc = String.valueOf(getRule(KingdomRule.NEW_TOWN_COST));
		String currency = ConfigManager.CURRENCY_NAME.get();
		String s = NotifyMessage.KINGDOM_RULE_READABLE.get("{NAME}|" + t.getName(), "{NEWTOWNCOST}|" + ntc, "{CURRENCY}|" + currency, "{DAILYTAX}|" + dtpc, "{MINIMUMCITIZENS}|" + minc, "{MAXIMUMCITIZENS}|" + maxc, "{RAIDINTERNALTOWNS}|"+ rit, "{ENEMYINTERNALTOWNS}|" + eit)[0];
		return s;
	}
}

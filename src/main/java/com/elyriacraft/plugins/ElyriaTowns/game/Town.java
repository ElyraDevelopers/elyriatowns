
package com.elyriacraft.plugins.ElyriaTowns.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.elyriacraft.plugins.ElyriaCore.utils.ChunkUtils;
import com.elyriacraft.plugins.ElyriaCore.utils.LocationUtils;
import com.elyriacraft.plugins.ElyriaTowns.EtMainClass;
import com.elyriacraft.plugins.ElyriaTowns.game.players.EtPlayer;
import com.elyriacraft.plugins.ElyriaTowns.game.players.Lord;
import com.elyriacraft.plugins.ElyriaTowns.permissions.TownPermission;
import com.elyriacraft.plugins.ElyriaTowns.utils.exceptions.TownNameNotUniqueException;


/**
 * The Class Town.
 */
/**
 * @author topisani
 *
 */
public class Town {

	/** The id. */
	private int id;
	
	/** The name. */
	private String name;
	
	/** The kingdom. */
	private Kingdom kingdom;
	
	/** The spawn. */
	private Location spawn;
	
	/** The claims. */
	private List<Claim> claims;
	
	/** The lord. */
	private Lord lord;
	
	/** The officers. */
	private List<EtPlayer> officers;

	/** The officers permissions. */
	private List<TownPermission> officerPermissions;

	/** The ConfigurationSection. */
	private ConfigurationSection cs;
	
	/** The plugin. */
	private static EtMainClass plugin = (EtMainClass)EtMainClass.getInstance();
	
	/** The map of the claims. */
	private Map<String, Object> map = plugin.getStorageMan().getClaimFile().getConfig().getValues(false);
	
	/**
	 * Instantiates a new town.
	 *
	 * @param id the id
	 */
	private Town(int id) {
		this.id = id;
		cs = plugin.getStorageMan().getTownFile().getConfig().createSection(String.valueOf(id));
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 * @throws TownNameNotUniqueException if the given name is not unique
	 */
	public void setName(String name) throws TownNameNotUniqueException{
		for(Town cur : plugin.getStorageMan().getTowns()) {
			if(cur.getName().equalsIgnoreCase(name)) throw new TownNameNotUniqueException("the town name is not unique. '" + name + "' is equal to '" + cur + "'!");
		}
		this.name = name;
	}
	
	/**
	 * Gets the spawn.
	 *
	 * @return the spawn
	 */
	public Location getSpawn() {
		return spawn;
	}
	
	/**
	 * Sets the spawn.
	 *
	 * @param spawn the location to set
	 */
	public void setSpawn(Location spawn) {
		this.spawn = spawn;
	}
	
	/**
	 * Gets the claims.
	 *
	 * @return the claims
	 */
	public List<Claim> getClaims() {
		return claims;
	}
	
	/**
	 * Gets the lord.
	 *
	 * @return the lord
	 */
	public Lord getLord() {
		return lord;
	}
	
	/**
	 * Sets the lord.
	 *
	 * @param lord the lord to set
	 */
	public void setLord(Lord lord) {
		this.lord = lord;
	}
	
	/**
	 * Gets the officers.
	 *
	 * @return the officers
	 */
	public List<EtPlayer> getOfficers() {
		return officers;
	}
	
	/**
	 * Adds an officer.
	 *
	 * @param officer the officer to add
	 */
	public void addOfficer(EtPlayer officer) {
		officers.add(officer);
	}
	
	/**
	 * Removes an officer from the list.
	 *
	 * @param officer the officer
	 * @return true, if successful
	 */
	public boolean removeOfficer(EtPlayer officer) {
		return officers.remove(officer);
	}
	
	/**
	 * Gets the officer permissions.
	 *
	 * @return the officerPermissions
	 */
	public List<TownPermission> getOfficerPermissions() {
		return officerPermissions;
	}

	/**
	 * Adds a TownPermission to the officer permissions.
	 *
	 * @param permission the TownPermission to add
	 */
	public void addOfficerPermission(TownPermission permission) {
		officerPermissions.add(permission);
	}
	
	/**
	 * Removes a TownPermission from the officer permissions.
	 *
	 * @param permission the permission to remove
	 * @return true, if successful
	 */
	public boolean removeOfficerPermission(TownPermission permission) {
		return officerPermissions.remove(permission);
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Gets the kingdom.
	 *
	 * @return the kingdom
	 */
	public Kingdom getKingdom() {
		return kingdom;
	}
	
	/**
	 * Sets the kingdom.
	 * This will <i>only</i> be used when creating the town, as the switching of kingdoms is imposible.
	 * @param kingdom the new kingdom
	 */
	private void setKingdom(Kingdom kingdom) {
		this.kingdom = kingdom;
	}
	
	/**
	 * Gets the ConfigurationSection.
	 *
	 * @return the ConfigurationSection
	 */
	public ConfigurationSection getConfSec() {
		return cs;
	}
	
	/**
	 * Claim.
	 *
	 * @param claim the claim
	 * @return true, if successful
	 */
	public boolean claim(Claim claim){
		if(claim.isClaimed()){
			return false;
		}
		if(claim.getKingdom() == getKingdom()){
			claims.add(claim);
			saveToFile();
			return true;
		}
		return false;
	}
	
	/**
	 * By claim.
	 *
	 * @param claim the claim
	 * @return the town
	 */
	public static Town byClaim(Claim claim){
		EtMainClass p = (EtMainClass)EtMainClass.getInstance();
		String cString = ChunkUtils.toString(claim);
		Map<String, Object> map = p.getStorageMan().getClaimFile().getConfig().getValues(false);
		if(map.containsKey(cString)) return new Town(Integer.parseInt((String)map.get(cString)));
		return null;
	}
	
	/**
	 * By id.
	 *
	 * @param id the id
	 * @return the town
	 */
	public static Town byId(int id) {
		if(plugin.getStorageMan().getTowns().contains(new Town(id))) return new Town(id);
		return null;
	}
	
	/**
	 * By name.
	 *
	 * @param name the name
	 * @return the town[]
	 */
	@SuppressWarnings("null")
	public static Town[] byName(String name) {
		FileConfiguration fc = plugin.getStorageMan().getTownFile().getConfig();
		Set<String> keys = fc.getKeys(false);
		Town[] matches = null;
		int i = -1;
		for(String cur: keys) {
			i++;
			String n;
			try {
				n = fc.getString(cur + ".name");
			} catch (NullPointerException e) {
				e.printStackTrace();
				continue;
			}
			if(n.matches(name)) {
				try {
					i = Integer.valueOf(cur);
				} catch (NumberFormatException e) {
					e.printStackTrace();
					continue;
				}
				matches[i] = new Town(i);
			}
		}
		return matches;
		
	}
	
	
	/**
	 * creates a new town.
	 * 
	 * @param name the name
	 * @param kingdom the kingdom
	 * @param lord the lord
	 * @throws TownNameNotUniqueException 
	 */
	public static Town create(String name, Kingdom kingdom, Lord lord) throws TownNameNotUniqueException{
		int i = 1;
		for(String s : plugin.getStorageMan().getTownFile().getConfig().getKeys(false)) {
			int cur = Integer.parseInt(s);
			if(i != cur) break;
			i++;
		}
		Town t = new Town(i);
		try {
			t.setName(name);
		} catch (TownNameNotUniqueException e) {
			plugin.getLog().debug(e.getMessage());
			throw e;
		}
		t.setLord(lord);
		t.setKingdom(kingdom);
		return t;
	}

	/**
	 * Save to file.
	 */
	public void saveToFile() {
		cs.set("name", name);
		cs.set("kingdom", kingdom.toString());
		cs.set("spawn", LocationUtils.toString(spawn));
		cs.set("lord", lord);
		List<String> off = new ArrayList<String>();
		for(EtPlayer cur : officers) {
			off.add(cur.getName());
		}
		cs.set("officers", off);
		for(Claim cur: claims){
			plugin.getStorageMan().getClaimFile().getConfig().set(ChunkUtils.toString(cur), id);
		}
	}
	public void loadFromFile() {
		lord = new Lord(cs.getString("lord"));
		name = cs.getString("name");
		kingdom = Kingdom.parseKingdom(cs.getString("kingdom"));
		for(Entry<String, Object> entry : map.entrySet()) {
			if((Integer) entry.getValue() == id) {
				claims.add((Claim) ChunkUtils.fromString(entry.getKey()));
			}
		}
	}
	
	/**
	 * Name list.
	 *
	 * @param towns the towns
	 * @return the string[]
	 */
	@SuppressWarnings("null")
	public static final String[] nameList(Town[] towns) {
		String[] s = null;
		for(int i = 0; towns.length < i; i++) {
			s[i] = towns[i].getName();
		}
		return s;
	}
}

package com.elyriacraft.plugins.ElyriaTowns.game;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.elyriacraft.plugins.ElyriaTowns.EtMainClass;
import com.elyriacraft.plugins.ElyriaTowns.game.rules.KingdomRules;
import com.elyriacraft.plugins.ElyriaTowns.storage.ConfigManager;

public enum Kingdom {
	
	NORTH,
	SOUTH,
	WEST,
	EAST,
	MIDDLE;
	
	private EtMainClass plugin = (EtMainClass)EtMainClass.getInstance();
	private ConfigurationSection cs = plugin.getStorageMan().getKingdomFile().getConfig().getConfigurationSection(this.toString());
	private Scoreboard sb = plugin.getScoreboard();
	public String getName(){
		return cs.getString("name");
	}
	public Team getTeam() {
		Team t;
		try {
			t = sb.registerNewTeam(this.toString().toLowerCase());
		} catch (IllegalArgumentException e) {
			t = sb.getTeam(this.toString().toLowerCase());
			return t;
		}
		
		return t;
	}
	public List<Player> getPlayers() {
		List<Player> lp = new ArrayList<Player>();
		FileConfiguration fc = plugin.getStorageMan().getPlayerFile().getConfig();
		for(String c : fc.getKeys(false)) {
			if(parseKingdom(fc.getString(c + ".kingdom")) == this) {
				lp.add(plugin.getServer().getPlayer(c));
			}
		}
		return lp;
	}
	public void setName(String name){
		cs.set("name", name);
	}
	public ConfigurationSection getConfSec(){
		return cs;
	}
	public KingdomRules getRules() {
		return new KingdomRules(this);
	}
	public static Kingdom parseKingdom(String s){
		switch (s){
			case "N": case "n": case "north": case "North": case "NORTH":
				return Kingdom.NORTH;
			case "S": case "s": case "south": case "South": case "SOUTH":
				return Kingdom.SOUTH;
			case "W": case "w": case "west": case "West": case "WEST":
				return Kingdom.WEST;
			case "E": case "e": case "east": case "East": case "EAST":
				return Kingdom.EAST;
			default:
				return Kingdom.MIDDLE;
		}	
	}
}

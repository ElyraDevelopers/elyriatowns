package com.elyriacraft.plugins.ElyriaTowns.game.rules;

public enum TownRule {

	DAILY_TAX,
	MAIN_FOCUS,
	WELCOME_MESSAGE,
}

package com.elyriacraft.plugins.ElyriaTowns.game.players;

import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import com.elyriacraft.plugins.ElyriaCore.plugin.game.EcPlayer;
import com.elyriacraft.plugins.ElyriaCore.plugin.permissions.Permission;
import com.elyriacraft.plugins.ElyriaCore.utils.exceptions.InventoryFullException;
import com.elyriacraft.plugins.ElyriaTowns.EtMainClass;
import com.elyriacraft.plugins.ElyriaTowns.game.Claim;
import com.elyriacraft.plugins.ElyriaTowns.game.Kingdom;
import com.elyriacraft.plugins.ElyriaTowns.game.Town;
import com.elyriacraft.plugins.ElyriaTowns.permissions.EtPermission;
import com.elyriacraft.plugins.ElyriaTowns.permissions.TownPermission;
import com.elyriacraft.plugins.ElyriaTowns.storage.ConfigManager;
import com.elyriacraft.plugins.ElyriaTowns.storage.StorageManager;

/**
 * The Class EtPlayer.
 */
public class EtPlayer extends EcPlayer{
	
	/** The storage manager. */
	private StorageManager storageManager;
    
    /** The player. */
    private OfflinePlayer player;
    
    /** The ConfigurationSection. */
    private ConfigurationSection cs;
    
    /** The plugin. */
    private static EtMainClass plugin = (EtMainClass)EtMainClass.getInstance();
    
    public EtPlayer(String playerName){
    	super(plugin.getServer().getOfflinePlayer(playerName));
        this.storageManager = plugin.getStorageMan();
        this.player = plugin.getServer().getOfflinePlayer(playerName);
        cs = storageManager.getPlayerFile().getConfig().getConfigurationSection(playerName.toLowerCase());
    }

    /**
     * Instantiates a new EtPlayer.
     *
     * @param player the player
     */
    public EtPlayer(OfflinePlayer player){
    	super(player);
        this.storageManager = plugin.getStorageMan();
        this.player = player;
        cs = storageManager.getPlayerFile().getConfig().getConfigurationSection(player.getName().toLowerCase());
    }
    
    /**
     * Gets the town.
     *
     * @return the town
     */
    public Town getTown() {
    	int i = cs.getInt("Town");
    	return new Town(i);
    }
    
    /**
     * Sets the town.
     *
     * @param t the new town
     */
    public void setTown(Town t) {
    	int tid = (t == null)? 0 : t.getId();
    	cs.set("Town", tid);
    }
    
    /**
     * Checks for town.
     *
     * @return true, if successful
     */
    public boolean hasTown() {
    	return getTown().getId() == 0;
    }
    
    /**
     * Gets the kingdom.
     *
     * @return the kingdom
     */
    public final Kingdom getKingdom() {
		switch (cs.getString("kingdom")){
			case "N":
				return Kingdom.NORTH;
			case "S":
				return Kingdom.SOUTH;
			case "W":
				return Kingdom.EAST;
			default:
				return Kingdom.MIDDLE;
		}
	}
    
    /**
     * Gets the permission list.
     *
     * @return the permission list
     */
    public final List<String> getPermList() {
        	return cs.getStringList("Permissions");
    }
    
    /**
     * Checks for permission.
     *
     * @param permission the permission
     * @return true, if successful
     */
    public final boolean hasPerm(Permission permission) {
    	if(permission instanceof TownPermission) {
    		if(isLord()) return true;
    	} else {
    		if(player.isOp()) return true;
    		if(player.getPlayer().hasPermission(permission.get())) return true;
    	}
    	return false;
    }
    
    /**
     * Checks if the player is standing in the given kingdom.
     *
     * @param kingdom the kingdom
     * @return true, if is standing in the kingdom
     */
    public final boolean isStandingIn(Kingdom kingdom) {
    	Kingdom k = new Claim(player.getPlayer().getLocation().getChunk()).getKingdom();
    	return (kingdom == k);
    }
    
    /**
     * Checks if the player is lord of a town.
     *
     * @return true, if player is lord
     */
    public final boolean isLord() {
    	return getTown().getLord() == this;
    }
    
    /**
     * Checks if the player is town officer.
     *
     * @return true, if the player is town officer
     */
    public final boolean isTownOfficer() {
    	return false;
    }
    /**
     * Give town block.
     *
     * @throws InventoryFullException if the inventory is full
     */
    public final void giveTownBlock() throws InventoryFullException{
    	int id = Integer.parseInt(ConfigManager.TOWN_BLOCK_ID.get());
    	byte data = Byte.parseByte(ConfigManager.TOWN_BLOCK_DATA.get());
    	MaterialData md = new MaterialData(id, data);
    	ItemStack i = new ItemStack(id);
    	i.setData(md);
    	try {
			this.giveItem(i);
		} catch (InventoryFullException e) {
			throw e;
		}
    }

    
    
    
    
    
    
    
    
    
}
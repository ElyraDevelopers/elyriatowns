package com.elyriacraft.plugins.ElyriaTowns.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.elyriacraft.plugins.ElyriaTowns.game.Claim;
import com.elyriacraft.plugins.ElyriaTowns.messages.NotifyMessage;

public class PlayerMove implements Listener{

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		Claim fl = new Claim(e.getFrom().getChunk());
		Claim tl = new Claim(e.getTo().getChunk());
		if(fl.getKingdom() != tl.getKingdom()){
			NotifyMessage.STEPED_OVER_KINGDOM_BORDER.sendTo(p, "{KINGDOM}|" + tl.getKingdom().getName());
		}
		//if(fl)
	}
}

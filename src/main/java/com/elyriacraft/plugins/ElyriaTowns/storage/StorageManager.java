package com.elyriacraft.plugins.ElyriaTowns.storage;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.elyriacraft.plugins.ElyriaCore.plugin.MainClassTemplate;
import com.elyriacraft.plugins.ElyriaCore.plugin.storage.CustomFileManager;
import com.elyriacraft.plugins.ElyriaCore.plugin.storage.StorageManagerTemplate;
import com.elyriacraft.plugins.ElyriaTowns.game.Town;
public class StorageManager extends StorageManagerTemplate{

	private CustomFileManager kingdomFile;
	private CustomFileManager townFile;
	private CustomFileManager playerFile;
	private CustomFileManager claimFile;

	public StorageManager(MainClassTemplate plugin) {
		super(plugin);
		kingdomFile = new CustomFileManager(plugin, "kingdoms");
		townFile = new CustomFileManager(plugin, "towns");
		playerFile = new CustomFileManager(plugin, "players");
		claimFile = new CustomFileManager(plugin, "claims");
	}

	@Override
	public void reloadAll() {	
		kingdomFile.reloadConfig();
		townFile.reloadConfig();
		playerFile.reloadConfig();
		claimFile.reloadConfig();
	}
	public CustomFileManager getKingdomFile(){
		return kingdomFile;
	}
	public CustomFileManager getTownFile(){
		return townFile;
	}
	public CustomFileManager getPlayerFile(){
		return playerFile;
	}
	public CustomFileManager getClaimFile(){
		return claimFile;
	}
	public List<Town> getTowns(){
		List<Town> towns = new ArrayList<Town>();
		Set<String> keys = townFile.getConfig().getKeys(false);
		for(String cur : keys){
			int curId = Integer.parseInt(cur);
			towns.add(new Town(curId));
		}
		return towns;
	}
}

package com.elyriacraft.plugins.ElyriaTowns.storage;

import org.bukkit.configuration.file.FileConfiguration;

import com.elyriacraft.plugins.ElyriaCore.plugin.MainClassTemplate;
import com.elyriacraft.plugins.ElyriaTowns.EtMainClass;

public enum ConfigManager {

	TOWN_BLOCK_ID("townblock.id", "100"),
	TOWN_BLOCK_DATA("townblock.data", "14"),
	CURRENCY_NAME("currency name", "$"),
	TOWN_CAN_BE_NEUTRAL("town can be neutral", "false");

	private String string = null;
	private Object defaultValue = null;
	private MainClassTemplate plugin = EtMainClass.getInstance();
	private FileConfiguration co = plugin.getConfig();

	private ConfigManager(String config, String defaultValue) {
		this.string = config;
		this.defaultValue = defaultValue;
		co.addDefault(string, defaultValue);
	}

	public String get() {
		return co.getString(string);
	}

	public void set(Object value) {
		co.set(string, String.valueOf(value));
		plugin.saveConfig();
	}

	public void addDefault() {
		co.addDefault(string, defaultValue);
	}

}

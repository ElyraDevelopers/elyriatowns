package com.elyriacraft.plugins.ElyriaTowns.utils.exceptions;

public class TownNameNotUniqueException extends Exception{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3296143149913281507L;

	/**
	 * Instantiates a new town name not unique exception.
	 */
	public TownNameNotUniqueException() {
		super();
	}
	
	/**
	 * Instantiates a new town name not unique exception.
	 *
	 * @param message the message
	 */
	public TownNameNotUniqueException(String message) {
		super(message);
	}


}

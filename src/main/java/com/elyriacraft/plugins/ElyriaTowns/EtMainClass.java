package com.elyriacraft.plugins.ElyriaTowns;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import com.elyriacraft.plugins.ElyriaCore.plugin.MainClassTemplate;
import com.elyriacraft.plugins.ElyriaTowns.storage.StorageManager;
import com.elyriacraft.plugins.ElyriaTowns.events.*;

public class EtMainClass extends MainClassTemplate{

	private StorageManager stman;
	private ScoreboardManager sbman = Bukkit.getScoreboardManager();
	private Scoreboard sb;
	public void onEnable(){
		stman = new StorageManager(this);
		sb = sbman.getMainScoreboard();
		getServer().getPluginManager().registerEvents(new PlayerMove(), this)
	}
	
	private static EtMainClass instance;{
		instance = this;
	}
	public static MainClassTemplate getInstance() {
		return instance;
	} 
	public StorageManager getStorageMan(){
		return stman;
	}
	public Scoreboard getScoreboard() {
		return sb;
	}
	protected static String name = "ElyriaTowns";
}

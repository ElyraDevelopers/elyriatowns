package com.elyriacraft.plugins.ElyriaTowns.permissions;
import com.elyriacraft.plugins.ElyriaCore.plugin.permissions.Permission;
public enum TownPermission implements Permission{

	/** The permission to claim land for the town */
	TOWN_CLAIM(false),
	;

	/** if the permission is default for the player */
	private boolean playerDefault; 
	
	/**
	 * Instantiates a new town permission.
	 *
	 * @param playerDefault the player default
	 */
	private TownPermission(boolean playerDefault){
		this.playerDefault = playerDefault;
	}
	
	/* (non-Javadoc)
	 * @see com.elyriacraft.plugins.ElyriaCore.plugin.permissions.Permission#get()
	 */
	@Override
	public String get() {
		return "ElyriaTowns." + this.toString().toLowerCase().replaceAll("_",".");
	}

	/* (non-Javadoc)
	 * @see com.elyriacraft.plugins.ElyriaCore.plugin.permissions.Permission#isDefault()
	 */
	@Override
	public boolean isDefault() {
		return playerDefault;
	}

}

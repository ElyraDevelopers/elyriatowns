package com.elyriacraft.plugins.ElyriaTowns.permissions;

import com.elyriacraft.plugins.ElyriaCore.plugin.permissions.Permission;

// TODO: Auto-generated Javadoc
/**
 * The Enum EtPermission.
 */
public enum EtPermission implements Permission{

	/** The permission to let players leave kingdoms. */
	PLAYER_KINGDOM_LEAVE(false),
	
	/** The permission to let players join kingdoms. */
	PLAYER_KINGDOM_JOIN(true),
	
	/** The permission to let players create towns. */
	PLAYER_TOWN_CREATE(true),
	
	/** The permission to let players leave towns. */
	PLAYER_TOWN_LEAVE(true),
	
	/** The permission to let players join towns. */
	PLAYER_TOWN_JOIN(true);
	
	/** if the permission is default for the player */
	private boolean playerDefault; 
	
	/**
	 * Instantiates a new EtPermission.
	 *
	 * @param playerDefault if the permission is default for the player
	 */
	private EtPermission(boolean playerDefault){
		this.playerDefault = playerDefault;
	}
	
	/* (non-Javadoc)
	 * @see com.elyriacraft.plugins.ElyriaCore.plugin.permissions.Permission#get()
	 */
	@Override
	public String get() {
		return "ElyriaTowns." + this.toString().toLowerCase().replaceAll("_",".");
	}

	/* (non-Javadoc)
	 * @see com.elyriacraft.plugins.ElyriaCore.plugin.permissions.Permission#isDefault()
	 */
	@Override
	public boolean isDefault() {
		return playerDefault;
	}
	

}
